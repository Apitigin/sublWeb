
import { Component, Vue } from 'vue-property-decorator';

import "./tabs.scss";
import {File} from "../../storage/file";
import {Editor} from "../editor/index";
import {isUndefined} from "util";
@Component({
  template: require('./tabs.html'),
  components: {
    "editor": Editor
  }
})


export class TabsComponent extends Vue {
  tabs: Array<File>;

  constructor() {
    super();
    this.tabs = new Array();

    this.tabs.forEach((value) => value.isActive = false);
    // test files
    this.tabs.push(new File("1",
`function a() {
    return 1;
}`, "javascript"));
    this.tabs.forEach((value) => value.isActive = false);

    this.tabs.push(new File("2", `function a(){
      return 2;
    }`, "javascript"));
    this.tabs.forEach((value) => value.isActive = false);
    this.tabs.push(new File("3", `function a(){
      return 3;
    }`, "javascript"));
    this.tabs.forEach((value) => value.isActive = false);
    this.tabs.push(new File("4", `function a(){
      return 4;
    }`, "javascript"));
  }
  setActive( tab: File ) {
    // deactivate all
    this.tabs.forEach(value => value.isActive = false);

    // activate focused tab
    tab.isActive = true;

    // safety focus
    if ( !isUndefined(tab.instance) )
      tab.instance.focus();
  }

  indexOf(tab: File, $childs: Editor[]) {
    let index = this.tabs.indexOf(tab);
    // this.tabs[index].instance = $childs[index];
    return index;
  }

  close(tab: File, $event = undefined) {
    if ( !isUndefined($event) )
      $event.stopPropagation();

    let itSelf = false;

    this.tabs.forEach(val => {
      if ( tab.isActive === true && val === tab )
        itSelf = true;
    });

    let index = this.tabs.indexOf(tab);

    if ( itSelf && !isUndefined(this.tabs[index - 1]))
      this.setActive(this.tabs[index - 1]);

    this.$nextTick(() => {
      // if ( !isUndefined(this.tabs[index].instance) )
      //   this.tabs[index].instance.destoroy();
      console.log(index);
      // Vue.delete(this.tabs, index);
      this.tabs.splice(index, 1);

      // init untitled page
      if ( this.tabs.length === 0 ) {
        this.initDefaultPage();
      }
    });
  }
  showClose(tab: File) {
    return !(this.tabs.length === 1 && tab.fileName === 'untitled' && tab.content === "");
  }

  initDefaultPage() {
    this.tabs.push(new File("untitled", '', 'javascript'));
  }
}
