import {Prop, Vue, Component} from 'vue-property-decorator';
import brace from "brace";
import { EditorDirective }from "./directive";
require('brace/theme/monokai');
require('brace/mode/javascript');

@Component({
  template: '<div :id="editorId" style="width: 100%; height: 100%;"></div>'
})

export class Editor extends Vue {

  @Prop([String])
  editorId: string;

  @Prop([String])
  content: string;

  @Prop({default: 'text'})
  lang: string;

  @Prop({default: 'text'})
  theme: string;

  editor;

  directive: EditorDirective;
  mounted() {
    this.editor = brace.edit(this.$el);
    this.editor.setTheme('ace/theme/monokai');
    this.editor.getSession().setMode('ace/mode/javascript');
    this.editor.setValue(this.content);
    this.editor.clearSelection();
    this.editor.setShowPrintMargin(false);
    this.directive = new EditorDirective(this);
  }

  focus(n: number = null) {
    this.editor.focus();
  }
  destoroy() {
    this.editor.destroy();
  }
}
