import { Vue, Component, } from 'vue-property-decorator';
import {MainDirective} from "./directive";
import './main.scss';
import {TabsComponent} from "../tabs/tabs";

@Component({
  template: require('./main.html'),
  components: {
    "tabs": TabsComponent
  }
})
export class MainComponent extends Vue {

  directive: MainDirective;
  mounted() {
    this.directive  = new MainDirective(this);
  }
}
