import $ from 'jquery';
import {MainComponent} from "./index";

export class MainDirective {

  $this: MainComponent;
  $el: JQuery<HTMLElement>;
  $spacer: JQuery<HTMLElement>;

  constructor($this: MainComponent) {
    this.$this = $this;
    this.$el = $(this.$this.$el);
    this.$spacer = this.$el.find('.spacer');

    this.$el.height($(window).height() - $('.menu-container').outerHeight());

    this.$el.find('.editors').height( this.$el.find('.content-edit').outerHeight() - this.$el.find("nav .tabs-ul").height() );
    $(window).on("resize", () => {
      this.$el.height($(window).height() - $('.menu-container').outerHeight() );
      this.$el.find('.editors').height( this.$el.find('.content-edit').outerHeight() - this.$el.find("nav .tabs-ul").height() );
    });

    this.resizeSpacer();
  }


  resizeSpacer(): void {
    this.$spacer.on("mousedown", () => {

      $(window).on('mousemove', (e: JQuery.Event) => {

        this.$el.find('.folder-place').width(e.pageX);
        this.$el.find('.content-edit').width($(window).width() - 3 - e.pageX);

        $(window).off('mouseup').on('mouseup', () => {
          $(window).off('mousemove');
        });
      });
    });
  }
}
