import $ from 'jquery';
import {MenuComponent} from "./index";

export class MenuDirective {

  private $this: MenuComponent;

  constructor($this: MenuComponent) {

    this.$this = $this;
    this.render();
  }

  render() {

    $(this.$this.$el)
      .find('>ul>li')
      .off('mouseenter')
      .on('mouseenter', this.liHover)
      .off('mouseleave')
      .on('mouseleave', this.liLeave.bind(this));
    $(document.body).off('click').on('click', this.deActive.bind(this));
  }

  liHover(): void {
    $(this).parent().find(">").each((k, v) => {
      $(v).removeClass('active');
    });
    $(this).addClass('active');
  }

  liLeave (): void {
    if (!this.$this.isActive) {
      $(this.$this.$el).find(".main-menu-item").each((k, v) => {
        $(v).removeClass('active');
      });
    }
  }

  deActive(e: JQuery.Event): void {
    if ( $(e.target).parents('.menu-container').length === 0 || $(e.target).parent().is('.menu-container') ) {
      this.$this.isActive = false;
      $(this.$this.$el).find(".main-menu-item").each((k, v) => {
        $(v).removeClass('active');
      });
    }
  }
}
