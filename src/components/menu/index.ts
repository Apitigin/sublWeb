import {Component, Vue} from 'vue-property-decorator';
import { MenuDirective } from './directive';
import './menu.scss';
import {isUndefined} from 'util';
import {MenuStorage} from "../../storage/menu";

@Component({
  template: require('./menu.html')
})

export class MenuComponent extends Vue {

  directive: MenuDirective;
  isActive = false;
  lies: {};
  storage: MenuStorage;

  constructor() {
    super();
    this.storage = MenuStorage.getInstance();
    this.lies = {};
    this.storage.getTransport().$on("add", this.addMenuItem);
  }

  public addMenuItem(name: string, to: string, callback: () => void): void {
    // set into main menu
    if ( to === '' ) {
      // first set into main menu
      if (isUndefined(this.lies[name]))
        this.lies[name] = {};
      this.lies[name].callback = callback;
    }
    // set into secondary menu;
    else {
      // check if not isset
      if ( isUndefined(this.lies[to]) )
        this.lies[to] = {};

      // check if not isset
      if ( isUndefined( this.lies[to].children))
        this.lies[to].children = {};

      this.lies[to].children[name] = {};
      this.lies[to].children[name].callback = callback;
    }
  }

  beforeMount() {
    this.storage.render();
  }

  mounted() {
    this.directive  = new MenuDirective(this);
  }

  setActive(node: any): void {
    if ( this.checkChild(node) )
      this.isActive = !this.isActive;
  }

  checkChild(node: any): boolean {
    return !isUndefined(node.children);
  }
}
