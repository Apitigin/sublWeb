import {Editor} from "../components/editor/index";

export class File {
  get instance(): Editor {
    return this._instance;
  }

  set instance(value: Editor) {
    this._instance = value;
  }

  get isActive(): boolean {
    return this._isActive;
  }

  set isActive(value: boolean) {
    this._isActive = value;
  }
  get lang(): string {
    return this._lang;
  }

  get content(): string {
    return this._content;
  }

  get fileName(): string {
    return this._fileName;
  }

  private _fileName: string;
  private _content: string;
  private _lang: string;
  private _isActive = true;
  private _instance: Editor;

  constructor(name: string, content: string, lang: string) {
    this._fileName = name;
    this._content = content;
    this._lang = lang;
  }



}
