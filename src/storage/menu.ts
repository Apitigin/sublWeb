import { Vue } from 'vue-property-decorator';
import {isUndefined} from "util";
export class MenuStorage {

  private static $instance;

  private $transport: Vue;

  private lies = {};

  public static getInstance(): MenuStorage {

    if ( isUndefined(MenuStorage.$instance) )
      MenuStorage.$instance = new MenuStorage();

    return MenuStorage.$instance;
  }

  public getTransport(): Vue {
    return this.$transport;
  }

  constructor() {
    this.$transport = new Vue();
  }

  public render(): void {

    this.addMenuItem('File', '', () => {
      alert('File');
    });
    this.addMenuItem('Edit', '', () => {
      alert('Edit');
    });
    this.addMenuItem('Selection', '', () => {

      alert('Selection');
    });
    this.addMenuItem('Find', '', () => {
      alert('Find');
    });
    this.addMenuItem('View', '', () => {
      alert('View');
    });
    this.addMenuItem('Goto', '', () => {
      alert('Goto');
    });
    this.addMenuItem('Tools', '', () => {
      alert('Tools');
    });
    this.addMenuItem('Project', '', () => {
      alert('Project');
    });
    this.addMenuItem('Preferences', '', () => {});
    this.addMenuItem('Help', '', () => {});
    this.addMenuItem('Eny1', 'Help', () => {
      alert('Eny1 - Help');
    });
    this.addMenuItem('Eny2', 'Help', () => {
      alert('Eny2 - Help');
    });
    this.addMenuItem('EnyPreferencesz', 'Preferences', () => {
      alert('EnyPreferencesz - Preferences');
    });
    this.addMenuItem('Eny3', 'Help', () => {
      alert('Eny3 - Help');
    });
    this.addMenuItem('Eny2', 'Preferences', () => {
      alert('Eny2 - Preferences');
    });
    this.addMenuItem('Eny4', 'Help', () => {
      alert('Eny4 - Help');
    });
    this.addMenuItem('Eny5', 'Help', () => {
      alert('catc0h - Help');
    });
  }

  public addMenuItem(name: string, to: string, callback: () => void): void {
    this.$transport.$emit("add", name, to, callback);
  }
}
