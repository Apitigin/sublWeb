import Vue from 'vue';
import { makeHot, reload } from './util/hot-reload';
import { createRouter } from './router';



const MenuComponent = () => import('./components/menu').then(({ MenuComponent }) => MenuComponent);
const navbarComponent = () => import('./components/navbar').then(({ NavbarComponent }) => NavbarComponent);
const MenuStorage = () => import('./storage/menu').then(({ MenuStorage }) => MenuStorage);
const MainComponent = () => import('./components/main').then(({ MainComponent}) => MainComponent);


import './sass/main.scss';

if (process.env.ENV === 'development' && module.hot) {
  const navbarModuleId = './components/navbar';
  const MenuComponentId = './components/menu';
  const MenuStorageId = './storage/menu';
  const MainComponentId = './components/menu';

  // first arguments for `module.hot.accept` and `require` methods have to be static strings
  // see https://github.com/webpack/webpack/issues/5668
  makeHot(navbarModuleId, navbarComponent, module.hot.accept('./components/navbar', () => reload(navbarModuleId, (<any>require('./components/navbar')).NavbarComponent)));
  makeHot(MenuComponentId, MenuComponent, module.hot.accept('./components/menu', () => reload(MenuComponentId, (<any>require('./components/menu')).MenuComponent)));
  makeHot(MenuStorageId, MenuComponent, module.hot.accept('./storage/menu', () => reload(MenuStorageId, (<any>require('./components/menu')).MenuStorage)));
  makeHot(MainComponentId, MainComponent, module.hot.accept('./components/main', () => reload(MainComponentId, (<any>require('./components/main')).MainComponent)));

}
new Vue({
  el: '#app-main',
  router: createRouter(),
  components: {
    'menu-component' : MenuComponent,
    'main-component': MainComponent
  }
});
